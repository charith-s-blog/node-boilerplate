import express from 'express';
import logger from './config/winston'
const app = express();
const port = process.env.PORT || 3000;

/**
 * @todo
 * add logger - winston
 * add test runner - mocha
 * add OAuth2 firewall
 * add sequelize to ORM with postgre
 */
app.use(logger);
app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Boilerplate app listening on port ${port}!`))