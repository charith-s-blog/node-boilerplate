import path from "path";
import {transports, format} from "winston";
import expressWinston from "express-winston";

let logger = expressWinston.logger({
    level: 'info',
    transports: [new transports.Console()],
    msg: `{{req.method}} {{req.url}} {{req.connection.remoteAddress}}`,
    format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.json(),
        format.printf(info =>{return `${info.level}: ${info.timestamp}  ${info.message}`;})),
});

export default logger;